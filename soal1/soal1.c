#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdint.h>
#include <dirent.h>

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

void build_decoding_table()
{

    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char)encoding_table[i]] = i;
}

void base64_cleanup()
{
    free(decoding_table);
}

char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length)
{

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = malloc(*output_length);
    if (encoded_data == NULL)
        return NULL;

    for (int i = 0, j = 0; i < input_length;)
    {

        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length)
{

    if (decoding_table == NULL)
        build_decoding_table();

    if (input_length % 4 != 0)
        return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=')
        (*output_length)--;
    if (data[input_length - 2] == '=')
        (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL)
        return NULL;

    for (int i = 0, j = 0; i < input_length;)
    {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6) + (sextet_b << 2 * 6) + (sextet_c << 1 * 6) + (sextet_d << 0 * 6);

        if (j < *output_length)
            decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length)
            decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}

int length = 4; // inisialisasi jumlah untuk looping

pthread_t tid[10]; // inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pid_t child;

int status;
int stateUnzip1, stateUnzip2, stateDecode, stateMove;
int stateFileno, StateUnziphasil;
void *Download(void *arg)
{
    status = 0;

    pid_t DL = fork();
    if (DL == 0)
    {
        execl("/bin/wget", "/bin/wget", "https://drive.google.com/u/1/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-q", "-O", "./music.zip", NULL);
        // exit(0);
    }
    else
    {
        int isDead;
        pid_t DL2 = fork();
        if (DL2 == 0)
        {
            execl("/bin/wget", "/bin/wget", "https://drive.google.com/u/1/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-q", "-O", "./quote.zip", NULL);
        }
        else
        {
            while ((wait(&isDead)) > 0)
                ;
            status = 1;
        }
    }

    return NULL;
}

void *unzipmusic(void *arg)
{
    while (status != 1)
    {
    }
    // status = 0;
    int isDead;

    stateUnzip1 = 0;
    pid_t prid = fork();
    printf("unzip is now running\n");
    // sleep(3);
    //   xecl("/bin/unzip", "/bin/unzip", "music.zip", "-d", "./hasil", NULL);
    if (prid == 0)
    {

        execl("/bin/unzip", "/bin/unzip", "-o", "./music.zip", "-d", "./music", NULL);
        printf("bru1\n");

        // exit(0);
    }
    else
    {
        // this is parent
        while ((wait(&isDead)) > 0)
            ;
        stateUnzip1 = 1;
    }

    // return NULL;
}

void *unzipquote(void *arg)
{
    while (status != 1)
    {
    }
    int isDead;
    stateUnzip2 = 0;
    pid_t prid = fork();
    printf("unzip is now running\n");
    // sleep(3);
    //   xecl("/bin/unzip", "/bin/unzip", "music.zip", "-d", "./hasil", NULL);
    if (prid == 0)
    {
        execl("/bin/unzip", "/bin/unzip", "-o", "./quote.zip", "-d", "./quote", NULL);

        printf("bru1\n");

        // exit(0);
    }
    else
    {
        // this is parent
        while ((wait(&isDead)) > 0)
            ;
        stateUnzip2 = 1;
    }

    // printf("%d\n", status);
    //  return NULL;
}

void *FileBase64(void *arg)
{
    while (stateUnzip1 != 1 && stateUnzip2 != 1)
    {
        // printf("%d\0", stateUnzip1);
        // printf("%d\0", stateUnzip2);
    }

    int isDead;
    stateDecode = 0;

    pid_t prid = fork();
    if (prid == 0)
    {
        execl("/bin/mkdir", "/bin/mkdir", "./hasil", NULL);
    }
    else
    {
        // this is parent
        while ((wait(&isDead)) > 0)
            ;
        printf("try to run\n");
        ListBase64("./music");

        ListBase64("./quote");

        stateDecode = 1;
    }

    return NULL;
}

void ListBase64(char *dirpath)
{

    DIR *dp;
    struct dirent *ep;
    printf("%s\n", dirpath);
    // char path[100];
    // strcpy(path, directory);

    dp = opendir(dirpath);

    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            FILE *fptr, *dest;
            char direction[30] = "";
            char filename[30] = "";
            strcat(direction, dirpath);
            strcat(direction, "/");
            strcat(direction, dirpath + 2);
            strcat(direction, ".txt");

            // printf("%s\n", direction);
            char path[30] = "";
            strcat(path, dirpath);
            strcat(path, "/");

            strcat(path, ep->d_name);
            fptr = fopen(path, "r");
            dest = fopen(direction, "a");

            // printf("%s\n", path);
            char data[30];
            fscanf(fptr, "%s", &data);
            // rintf("%s\n", data);
            long decode_size = strlen(data);
            char *decoded_data = base64_decode(data, decode_size, &decode_size);
            if (decoded_data != NULL)
            {
                fprintf(dest, "%s\n", decoded_data);
            }
            fclose(fptr);
            fclose(dest);
        }

        (void)closedir(dp);
    }
    else
        perror("Couldn't open the directory");
}

void *Move(void *arg)
{
    while (stateDecode != 1)
    {
        /* code */
    }

    stateMove = 0;
    int isDead;

    pid_t prid = fork();
    // printf("statemove%d\n", stateMove);

    if (prid == 0)
    {
        execl("/bin/mv", "/bin/mv", "-f", "./music/music.txt", "./hasil/music.txt", NULL);
    }
    else
    {

        // while ((wait(&isDead)) > 0)
        //     ;
        int isDead2;
        pid_t prid2 = fork();
        if (prid2 == 0)
        {
            execl("/bin/mv", "/bin/mv", "-f", "./quote/quote.txt", "./hasil/quote.txt", NULL);
        }
        else
        {
            while (wait(&isDead2) > 0)
                ;
            int isDead3;
            pid_t prid3 = fork();
            if (prid3 == 0)
            {

                execl("/bin/zip", "/bin/zip", "-m", "-P", "mihinomenestbilly", "-r", "hasil.zip", "./hasil", NULL);
            }
            else
            {

                while ((wait(&isDead3)) > 0)
                    ;
                stateMove = 1;
                printf("o%d\n", stateMove);
            }
        }
    }
}

void *UnzipHasil(void *arg)
{

    while (stateMove != 1)
    {
        /* code */
    }
    int isDead;
    pid_t prid = fork();
    if (prid == 0)
    {

        execl("/bin/unzip", "/bin/unzip", "-P", "mihinomenestbilly", "./hasil.zip", NULL);
    }
    else
    {

        while ((wait(&isDead)) > 0 && stateFileno != 1)
            ;
        int isDead2;
        pid_t prid2 = fork();
        if (prid2 == 0)
        {
            execl("/bin/zip", "/bin/zip", "-m", "-P", "mihinomenestbilly", "-r", "hasil.zip", "./hasil", "./no.txt", NULL);
        }
        else
        {
            while (wait(&isDead2))
                ;
            printf("ooogaboofafafa\n");
        }
    }
}
void *MakeFile(void *arg)
{
    while (stateMove != 1)
    {
        /* code */
    }
    FILE *fptr;
    stateFileno = 0;

    fptr = fopen("./no.txt", "w");

    fprintf(fptr, "No\n");
    fclose(fptr);
    stateFileno = 1;

    // fptr = fopen(path, "r");
    // dest = fopen(direction, "a");
}

void main()
{

    pthread_create(&(tid[0]), NULL, Download, NULL);
    pthread_create(&(tid[1]), NULL, unzipmusic, NULL);
    pthread_create(&(tid[2]), NULL, unzipquote, NULL);

    pthread_create(&(tid[3]), NULL, FileBase64, NULL);
    pthread_create(&(tid[4]), NULL, Move, NULL);
    pthread_create(&(tid[5]), NULL, UnzipHasil, NULL);
    pthread_create(&(tid[6]), NULL, MakeFile, NULL);

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_join(tid[2], NULL);
    pthread_join(tid[3], NULL);
    pthread_join(tid[4], NULL);
    pthread_join(tid[5], NULL);
    pthread_join(tid[6], NULL);

    exit(0);
}