# soal-shift-sisop-modul-3-F02-2022

Sisop modul 3

# Group

1. **Billy Brianto**(5025201080)
2. Amsal Herbert(5025201182)
3. Aqil Ramadhan Hadiono(5025201261)

# Dasar Penyelesaian

1. Novak

   Dasar penyelesaian soal pertama adalah fork, exec dan thread. Forking digunakan untuk emnjalankan command exec pada child process. Tetapi, terdapat suatu permasalahan dimana parent akan berjalan duluan dari childnya. Maka dari itu, status child dipantau san ditunggu sampai terminate baru menjalankan command lain. Untuk thread digunakan untuk masing masing fungsi, dimana dua fungsi dapat dijalankan secara skeuensial. Thread juga membutuhkan resource satu dengan yang lainnya, maka pada thread juga dilakukan pemantauan samapai status yang dipantau dpat berjalan.

2. Bluemary

   Dasar penyelesaian nomor 2 adalah client server communication menggunakan thread. Yang pertama dilakukan adalah membuat socket dan connect/listen, kemudian masuk kedalam langkah thread untuk connect. Thread digunakan dalam server, dimana server akan mengassign masing masing client kedalam satu thread. Untuk komunikai antar client server digunakan fungsi write dan read.

3. Nami
   Dasar penyelesaian nomor 3 adalah client server communication kembali, akan tetapi kali ini yang ingin dilakukan adalah transfer file. Pada soal ini transfer file dilakukan setelah menunggu command dari client yang berisi nama file. Transfer file dilakukan dengan read dari client dan write dari server.

# Penjelasan

## 1. Novak

Untuk soal ini, masalah yang ditemukan adalah thread yang membutuhkan suatu resource tidak dapat berjalan sebelum resource tersebut dibuat. SOlusi untuk masalh ini adalh mute sederhana dimana thread harus menunggu satu sama lain.

```c
    status = 0;
    pid_t DL = fork();
    if (DL == 0)
    {
        //command exec disini
    }
    else
    {
        int isDead;
        pid_t DL2 = fork();
        if (DL2 == 0)
        {
            //command exec disini
        }
        else
        {
            while ((wait(&isDead)) > 0)
                ;
            status = 1;
        }
    }

    return NULL;
```

untuk menangani command dengan exec, perlu dilakukan fork untuk mebuat proses baru. Struktur fork akan dilakukan seperti diatas, pada ujung fork(else terkahir/parent process) akan dilakukan wait sampai child process telah dieksekusi. Hal ini dilakukan untuk memastikan bahwa bagian selanjutnya dapt dilakukan setelahnya. Untuk bagian selanjutnya sinyal status akan dinyalakan untuk menunjukan thread yang berisi proses ini telah selesai.

```c
while (status != 1)
{
}
```

Untuk menunggu thread lain selesai, dapat diimplementasikan while loop yang akan berjalan jika satusnya tidak memnuhi syarat.

### Main

Dalam fungsi main, dibuat 7 buah thread yang akan menunggu satu sama lain dengan mutex pada tiap thread.

### Download & Unzip

Unatuk bagian download akan dilakukan dua command dengan exec. Hal ini akan dilakukan dengan wget dan fork. Setelah itu parent process akan menunggu sampai childnya selesai untuk menyalakan sinyal status bahwa dirinya telah selesai.

Selanjutnya, dua thread akan dijalankan untuk unzip file. Kedua thread akan menunggu sampai int status bernilai 1 untuk menjalankan keduanya sehingga unzip dilakukan secara bersamaan.

#### Unzipmusic & Unzipquote

```c
void *unzipquote(void *arg)
{
    while (status != 1)
    {
    }
    int isDead;
    stateUnzip2 = 0;
    pid_t prid = fork();
    printf("unzip is now running\n");
    if (prid == 0)
    {
        execl("/bin/unzip", "/bin/unzip", "-o", "./quote.zip", "-d", "./quote", NULL);

        printf("bru1\n");

    }
    else
    {
        // this is parent
        while ((wait(&isDead)) > 0)
            ;
        stateUnzip2 = 1;
    }
}
```

Kedua unzip music dan unzip quote akan dilakukan bersamaan dengan menunggu status download sampai menjadi 1. Struktur kedua program seperti snippet code diatas hnaya saja untuk unzipmusic variabel yang akan ditrack untuk berikutnya adalah sateUnzip1.

### Decode & pisahkan

```c
void *FileBase64(void *arg)
{
    while (stateUnzip1 != 1 && stateUnzip2 != 1)
    {
        // printf("%d\0", stateUnzip1);
        // printf("%d\0", stateUnzip2);
    }

    int isDead;
    stateDecode = 0;

    pid_t prid = fork();
    if (prid == 0)
    {
        execl("/bin/mkdir", "/bin/mkdir", "./hasil", NULL);
    }
    else
    {
        // this is parent
        while ((wait(&isDead)) > 0)
            ;
        printf("try to run\n");
        ListBase64("./music");

        ListBase64("./quote");

        stateDecode = 1;
    }

    return NULL;
}
```

Fungsi ini akan menunggu samapi Kedua state unzip selesai untuk berjalan. Tujuan fungsi ini adalah untuk membuat directory baru untuk tempat hasil encode yang dipindahlan. Setelah proses tersebut berhenti, akan dijalankan Dua fungsi Listbase64 untuk music dan quote. Akhirnya, flag decode akan dinyalakan untuk menunjukan bahwa state decode selesai.

#### Fungsi ListBase64

```c
oid ListBase64(char *dirpath)
{

    DIR *dp;
    struct dirent *ep;
    printf("%s\n", dirpath);
    // char path[100];
    // strcpy(path, directory);

    dp = opendir(dirpath);

    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            FILE *fptr, *dest;
            char direction[30] = "";
            char filename[30] = "";
            strcat(direction, dirpath);
            strcat(direction, "/");
            strcat(direction, dirpath + 2);
            strcat(direction, ".txt");

            // printf("%s\n", direction);
            char path[30] = "";
            strcat(path, dirpath);
            strcat(path, "/");

            strcat(path, ep->d_name);
            fptr = fopen(path, "r");
            dest = fopen(direction, "a");

            // printf("%s\n", path);
            char data[30];
            fscanf(fptr, "%s", &data);
            // rintf("%s\n", data);
            long decode_size = strlen(data);
            char *decoded_data = base64_decode(data, decode_size, &decode_size);
            if (decoded_data != NULL)
            {
                fprintf(dest, "%s\n", decoded_data);
            }
            fclose(fptr);
            fclose(dest);
        }

        (void)closedir(dp);
    }
    else
        perror("Couldn't open the directory");
}
```

Pada dasarnya, fungsi ini akan menerima directory path(./ music atau ./quote) yang berisi text file yang diencode. Tiap isi directory akan dibaca filenya dan diencode dengan base64. Selanjutnya semua isi masing-masing file akan di append ke file baru yang sesuai dengan pathnya.

### Pindahkan ke folder

```c
void *Move(void *arg)
{
    while (stateDecode != 1)
    {
        /* code */
    }

    stateMove = 0;
    int isDead;

    pid_t prid = fork();
    // printf("statemove%d\n", stateMove);

    if (prid == 0)
    {
        execl("/bin/mv", "/bin/mv", "-f", "./music/music.txt", "./hasil/music.txt", NULL);
    }
    else
    {

        // while ((wait(&isDead)) > 0)
        //     ;
        int isDead2;
        pid_t prid2 = fork();
        if (prid2 == 0)
        {
            execl("/bin/mv", "/bin/mv", "-f", "./quote/quote.txt", "./hasil/quote.txt", NULL);
        }
        else
        {
            while (wait(&isDead2) > 0)
                ;
            int isDead3;
            pid_t prid3 = fork();
            if (prid3 == 0)
            {

                execl("/bin/zip", "/bin/zip", "-m", "-P", "mihinomenestbilly", "-r", "hasil.zip", "./hasil", NULL);
            }
            else
            {

                while ((wait(&isDead3)) > 0)
                    ;
                stateMove = 1;
                printf("o%d\n", stateMove);
            }
        }
    }
}
```

Untuk fungsi ini, akan dilakukan forking sekuensial dengan wait status. Forking ini akan berisi fungsi mv untuk memindahkan hasil encode dari tiap folder quote atau music ke folder hasil. Selanjutnya akan dilakukan command dengan exec untuk zip.

```c
execl("/bin/zip", "/bin/zip", "-m", "-P", "mihinomenestbilly", "-r", "hasil.zip", "./hasil", NULL);
```

a
Pada akhirnya, terdapat flag untuk menunjukan statemove sudah selesai.

### Unzip, tambahkan file, dan zip

```c
void *UnzipHasil(void *arg)
{

    while (stateMove != 1)
    {
        /* code */
    }
    int isDead;
    pid_t prid = fork();
    if (prid == 0)
    {

        execl("/bin/unzip", "/bin/unzip", "-P", "mihinomenestbilly", "./hasil.zip", NULL);
    }
    else
    {

        while ((wait(&isDead)) > 0 && stateFileno != 1)
            ;
        int isDead2;
        pid_t prid2 = fork();
        if (prid2 == 0)
        {
            execl("/bin/zip", "/bin/zip", "-m", "-P", "mihinomenestbilly", "-r", "hasil.zip", "./hasil", "./no.txt", NULL);
        }
        else
        {
            while (wait(&isDead2))
                ;
            printf("ooogaboofafafa\n");
        }
    }
}
```

Bagian ini akan menunggu sampai move telah selesai, lalu file akan diunzip. Step selanjutnya adalah menunggu sampai File telah dibuat agar bisa melakukan zip kenbali.

#### Makefile

```c
void *MakeFile(void *arg)
{
    while (stateMove != 1)
    {
        /* code */
    }
    FILE *fptr;
    stateFileno = 0;

    fptr = fopen("./no.txt", "w");

    fprintf(fptr, "No\n");
    fclose(fptr);
    stateFileno = 1;

    // fptr = fopen(path, "r");
    // dest = fopen(direction, "a");
}
```

Fungsi Makefile akan menunggu sampai move selesai untuk membuat file dengan nama no yang berisi kata No.Setelah selesai flag stateFileno akan dinyalakan untuk dapat melanjutkan ke step zip.

## 2. Bluemary

Untuk soal ini, diminta untuk membuat multithread server dengan command yang diterima dari client. Untuk itu, digunakan thread

### Server

#### Main

Fungsi main dalam file server berguna untuk menjadi driver program dan membuat server seseuai port. Pertama, socket akan dibuat, kemudian server address akan diassign ke masing-masing parameter. Selanjutnya akan dilakukan bind. Untuk tahap listen, karena permintaan server yang dapat menerima multiple user maka dibuatlah sebuah while loop dimana server dapat melakukan accept. Jika ada sebuah koneksi yang dapat diterima, thread akan dibuat dan fungsi func akan diassign ke thread tersebut. Untuk menunggu thread berjalan, juga dilakukan join thread.

#### func

Fungsi ini berisi sebuah for loop yang terus berjalan untuk membaca input terus menerus dari user. Hal Didalamnya terdapat 4 response server terhadap permintaan user.

0. Quit

   Command ini akan djalankan ketika client memilih opsi 0 dan akan langsung menterminate current thread yang skearang berjalan.

1. Login

   Command ini akan dijalankan ketika client meminta prompt untuk login dan akan memanggil fungsi bool Login()

2. Register

   Command ini akan dijalankan ketika client meminta register dan langsung memanggil fungsi Register().

3. Skip

   Jika tidak ada opsi yang dipilih/input client random, opsi ini akan berjalan dan langsung melakukan continue terhadap for loop.

Kempat command tersebut akan mengirimkan sinyal status ke user lewat fungsi write yang berupa 1 untuk sukses dan 0 untuk gagal.

Jika user berhasil masuk, terdapat 1 buah while loop untuk membaca input command(add, see, download, submit).

1. Add

   Command ini akan membaca input dari client yang berupa judul, desc, input, output. Keempatinput ini akan kemudian dipass ke fungsi AddFile()

2. See

   Command ini akan langsung memanggil fungsi SeeFIle() untuk melihat list problem.

3. Download

   Command ini akan membaca input dari client yang merupakan nama problem yang akan didownload. Kemudian string ini akan dipass ke fungsi DownloadFIle().

4. Submit

   Command ini akan membaca dua input dari client, yaitu nama problem dan path answer yang akan dicek dari client. Kedua string ini akan kemudian dipass ke fungsi SubmitFIle().

#### Register

```c
void Register(char *username, char *password, int connfd)
{
    char compound[160] = "";
    FILE *in_file;
    in_file = fopen("users.txt", "a+");
    if (in_file == NULL)
    {
        printf("Error file missing\n");
        exit(-1);
    }
    if (UserValidator(username) && PassValidator(password))
    {

        strcat(compound, username);
        strcat(compound, "-");
        strcat(compound, password);
        fprintf(in_file, "%s\n", compound);
        printf("Register Success!");
        /* code */
    }
    else
    {
        printf("Failed to verify, try again!");
    }
    fclose(in_file);
}
```

Fungsi ini berguna untuk melakukan register dan validasi. Yang pertama dilakukan adalah melakukan validasi user dengan dua fungsi, yaitu:

1. PassValidator

   Fungsi yang berguna untuk validasi password dimana masing masing char password akan dibaca untuk mengkonfirasi apakah password valid atau tidak.

2. Uservalidator

   Fungsi yang berguna untuk melakukan validasi dan akan menolak user duplikat.

Jika register berhasil, maka username dan password akan tercatat dalam users.txt dengan format "username-password". Hal tersebut akan memudahkan proses login untuk mencocokan user dan password.

#### Login

```c
bool Login(char *username, char *password, int connfd)
{
    char *string[50];
    char compound[160] = "";
    FILE *in_file;
    in_file = fopen("users.txt", "a+");
    if (in_file == NULL)
    {
        printf("Error file missing\n");
        exit(-1);
    }

    strcat(compound, username);
    strcat(compound, "-");
    strcat(compound, password);
    // fprintf(in_file, "%s\n", compound);
    while (!feof(in_file))
    {
        fscanf(in_file, "%s", string);

        if (strstr(string, compound))
        {

            printf("Login Success\n");
            fclose(in_file);
            return true;
        }
    }
    printf("Login Failed!");
    /* code */

    printf("Failed to verify Login try again!");

    fclose(in_file);
    return false;
}
```

Untuk tahap Login, hal yang akan dicocokan hanyalah input user password dan string dalam file users.txt yang berupa "username-password". Jika tidak ada string yang cocok, login akan ditolak.

#### CreateFile

```c
void CreateFile()
{
    FILE *in_file;
    in_file = fopen("problems.tsv", "a+");
    if (in_file == NULL)
    {
        printf("Error file missing\n");
        exit(-1);
    }
    fclose(in_file);
}
```

Pada soal ini, diminta juga untuk membuat file problems.tsv untuk database problem pada awal saat server dijalankan. Fungsi ini hanya akan membuat file tersebut dan melakukan return.

#### AddFile

```c
void AddFile(char *author, char *judul, char *desc, char *input, char *output)
{

    struct stat st = {0};

    if (stat(judul, &st) == -1)
    {
        mkdir(judul, 0700);
    }
    char path[100] = "Server/";
    char path_cpy[100] = "";
    chdir("..");

    strcat(path, judul);
    strcat(path, "/");
    strcpy(path_cpy, path);

    // execl("/bin/pwd", "/bin/pwd", NULL);

    FILE *DescFile;
    DescFile = fopen(desc, "a+");

    FILE *InputFile;
    InputFile = fopen(input, "r+");

    FILE *OutputFile;
    OutputFile = fopen(output, "r+");

    FILE *SrvDesc;
    FILE *SrvIn;
    FILE *SrvOut;
    FILE *problemsTSV;

    strcat(path, "description.txt");
    SrvDesc = fopen(path, "w");
    strcpy(path, path_cpy);
    strcat(path, "input.txt");
    SrvIn = fopen(path, "w");
    strcpy(path, path_cpy);
    strcat(path, "output.txt");
    SrvOut = fopen(path, "w");

    // printf("%s", DescFile);
    problemsTSV = fopen("Server/problems.tsv", "a+");
    char Contents[300] = "";

    while (fscanf(DescFile, "%s", Contents) == 1)
    {
        fprintf(SrvDesc, "%s\n", Contents);
    }
    Contents[300] = '\0';
    while (fscanf(InputFile, "%s", Contents) != EOF)
    {
        fprintf(SrvIn, "%s\n", Contents);
    }
    Contents[0] = '\0';
    while (fscanf(OutputFile, "%s", Contents) != EOF)
    {
        fprintf(SrvOut, "%s\n", Contents);
    }
    Contents[0] = '\0';
    strcat(Contents, judul);

    strcat(Contents, "\t");
    strcat(Contents, author);
    printf("jdudl: %s\n", judul);

    printf("%s\n", Contents);
    fprintf(problemsTSV, "%s\n", Contents);

    fclose(DescFile);
    fclose(InputFile);
    fclose(OutputFile);
    fclose(SrvDesc);
    fclose(SrvIn);
    fclose(SrvOut);
    fclose(problemsTSV);

    chdir("Server");
    // execl("/bin/pwd", "/bin/pwd", NULL);
}
```

Fungsi ini berguna untuk melakukan add new problem pada database dan memerlukan input author, judul problem, file deskripsi, input, dan output.
Hal pertama yang dilakukan adalah membuat file sesuai Judul problem pada server. Kemudian directory akan dipindah menjadi directory soal untuk memnuhi permintaan input yang berupa "Client/[i/o/desc]". Selanjutnya, dibuka 6 buah file, 3 file yang berasal dari client, 3 buah sisanya(input, output desc didalam folder judul) adalah file dalam database untuk pencatatan. Untuk pencatatan sendiri, dilakukan 3 buah while loop yang akan melakukan copy dari file client ke database. Problem yang sudah tercatat selanjutnya akan dimasukan ke satu buah file ekstra yang berupa problems.tsv yang memiliki format "judul\tauthor". Akhirnya, direcotry dikembalikan ke directory server.

#### SeeFile

```c
void SeeFile(int conn)
{
    FILE *problemsTSV;
    problemsTSV = fopen("problems.tsv", "a+");
    char problem[100], auth[100];
    while (fscanf(problemsTSV, "%s\t%s\n", problem, auth) != EOF)
    {
        strcat(problem, " by ");
        strcat(problem, auth);
        char fullname[80] = "";
        printf("%s\n", problem);
        strcpy(fullname, problem);
        write(conn, fullname, sizeof(fullname));
    }
    write(conn, "break", sizeof("break"));
    printf("break otw\n");

    fclose(problemsTSV);
}
```

Fungsi SeeFile berguna untuk mengirimkan list file ke client. Untuk itu fungsi ini akn melakukan scan line by line terhadap problems.tsv(database problem) dan akan mengirimkannya ke client dengan fungsi write(). Pada akhir fungsi ini dilakukan 1 buah write extra yang berupa sinyal stop(string "break" pada progrma server) while loop untuk client side.

#### DownloadFile

```c
void DownloadFile(int connfd, char *foldername)
{
    DIR *dp;
    struct dirent *ep;
    chdir("..");
    char path[100] = "Server/";
    strcat(path, foldername);
    dp = opendir(path);
    strcat(path, "/");

    // create folder in client
    char clientFolder[100] = "Client/";
    strcat(clientFolder, foldername);
    mkdir(clientFolder, 0700);
    char freshpath[100] = "";
    char filePath[100] = "Client/";
    strcat(filePath, foldername);
    strcat(filePath, "/");
    strcpy(freshpath, filePath);
    printf("%s", filePath);
    char freshpathOR[100] = "";
    char filePathOR[100] = "Server/";
    strcat(filePathOR, foldername);
    strcat(filePathOR, "/");
    strcpy(freshpathOR, filePathOR);
    printf("%s", filePathOR);

    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (strcmp(ep->d_name, "description.txt") == 0 || strcmp(ep->d_name, "input.txt") == 0)
            {
                // pathfile[0] = '\0';
                strcat(filePathOR, ep->d_name);

                strcat(filePath, ep->d_name);
                FILE *fromfile,
                    *tofile;
                fromfile = fopen(filePathOR, "a+");
                tofile = fopen(filePath, "a+");
                char str[100];

                while (fscanf(fromfile, "%s\n", str) != EOF)
                {
                    fprintf(tofile, "%s\n", str);
                    str[0] = '\0';
                }

                strcpy(filePath, freshpath);
                strcpy(filePathOR, freshpathOR);
                puts(ep->d_name);
                fclose(fromfile);
                fclose(tofile);
                /* code */
            }
        }
        write(connfd, "Download Success!", sizeof("Download Success!"));
        (void)closedir(dp);
    }
    else
    {
        write(connfd, "Directory error!", sizeof("Directory error!"));

        perror("Couldn't open the directory");
    }

    chdir("Server");
}

```

Seperti namanya, fungsi DownloadFIle akan mendownload suaatu problem dari server dan mengirimkannya ke client. Argumen yang dimianta adalah connection id dan nama problem ayng ingin didownload. Proses download dapat dilakukan dengan copy seluruh file dalam folder problem yang diminta. Hal yang pertama dilakukan dalam fungsi ini adalah membuat directory dengan nama problem. Kemudia path untuk server dan client akan diconstruct dan directory problem pada server akan dibuka dan dibaca untuk masing-masing file didalamnya. Tiap file yang dibaca didalam folder problem isinya akan dicopy kedalam file yang bernama sama dalam client.

#### SubmitFile

```c
void SubmitFile(int connfd, char *foldername, char *cloutputfile)
{
    chdir("..");
    FILE *clout, *srvans;
    clout = fopen(cloutputfile, "a+");

    char path[100] = "Server/";
    strcat(path, foldername);
    // assume every problems has the same out name
    strcat(path, "/output.txt");
    printf("%s %s\n", path, cloutputfile);
    srvans = fopen(path, "a+");

    if (compareFileLenght(clout, srvans) && compareFiles(clout, srvans))
    {
        printf("Problem %s AC\n", foldername);
        write(connfd, "AC", sizeof("AC"));
    }
    else
    {
        printf("Problem %s WA\n", foldername);
        write(connfd, "WA", sizeof("WA"));

        /* code */
    }

    chdir("Server");
}
```

Fungsi SubmitFIle berguna untuk melakukan submit output problem client dan mebandingkannya ke server. Untuk mencapai hal tersebut directory akan kembali diubah ke file soal3, kemudian file output server dan client akan dibuka dan keduanya akan dipass ke dua fungsi berbeda, yaitu:

1. compareFileLenght

   Fungsi ini berguna untuk mebandingkan panjang isi dari file, jika panjang isi tidak sama, akan direturn false.

2. compareFIles

   Fungsi ini berguna untuk melakukan compare content, dimana tiap line akan dibaca dan dibandingkan satu sama lain.

Jika kedua fungsi validasi tersebut mereturn true, server akan mengirimkan string "AC" ke client. Sebaliknya jika validasi salah server akan mengirimkan string "WA".
Terakhir, directory akan dipindahkan kembali ke directory Server.

### Client

Dalam problem ini, semua fungsi command berada pada server, sehingga yang dilakukan client hanyalah melakukan read response server dan write untuk mengirimkan command. Hal ini dilaksanakan oleh sebuah fungsi yang bernama func.

#### Main

```c
int main()
{
    int sockfd, connfd;
    struct sockaddr_in srvaddr, cli;
    char st[30], us[30];
    // scanf("%s %s", us, st);
    // Register(us, st);
    // Login(us, st);
    // execl("/bin/pwd", "/bin/pwd", NULL);

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&srvaddr, sizeof(srvaddr));

    // assign IP, PORT
    srvaddr.sin_family = AF_INET;
    srvaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    srvaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (SA *)&srvaddr, sizeof(srvaddr)) != 0)
    {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");

    // function for chat
    func(sockfd);

    // close the socket
    close(sockfd);
}
```

Fungsi main pada program client berguna untuk membuat socket dan melakukan koneksi ke server. Hal pertama yang dilakukan adalah membuat sebuah socket dan mengassign server address. Kemudian client akan berusaha untuk melakukan connect ke server. Jika berhasil, fungsi func akan dijalankan, jika tidak program akan langsung exit.

#### Func

```c
void func(int sockfd)
{
    char buffer[MAX];
    char username[MAX];
    char password[MAX];
    char response[MAX];
    char command[MAX];
    char n[MAX];
    for (;;)
    {

        printf("\tOptions\n0. Exit\n1. Login\n2. Register\n");

        // send(sockfd, n, sizeof(n), 0);
        scanf("%s", n);
        write(sockfd, n, sizeof(n));
        if (strcmp(n, "0") == 0)
        {
            printf("Client Exit...\n");
            break;
        }
        scanf("%s", username);
        scanf("%s", password);
        write(sockfd, username, sizeof(username));
        write(sockfd, password, sizeof(password));
        read(sockfd, response, sizeof(username)); // read server status
        if (strcmp(response, "0") == 0)
        {
            continue;
        }
        while (strcmp(command, "quit") != 0)
        {
            printf("Input command: \n");

            scanf("%s", command);
            write(sockfd, command, sizeof(command));
            if (strcmp(command, "add") == 0)
            {
                printf("[Server] add\n");

                char Judul[30] = "";
                char Desc[30] = "";
                char Input[30] = "";
                char Output[30] = "";

                printf("Judul problem: ");
                scanf("%s", Judul);
                printf("Filepath description.txt: ");
                scanf("%s", Desc);

                printf("Filepath input.txt: ");
                scanf("%s", Input);

                printf("Filepath output.txt: ");
                scanf("%s", Output);

                write(sockfd, Judul, sizeof(command));
                write(sockfd, Desc, sizeof(command));
                write(sockfd, Input, sizeof(command));
                write(sockfd, Output, sizeof(command));
            }
            else if (strcmp(command, "see") == 0)
            {
                printf("[Server] see\n");
                char hasil[80];
                while (1)
                {
                    hasil[0] = '\0';
                    read(sockfd, hasil, sizeof(hasil));
                    printf("%s\n", hasil);
                    if (strcmp(hasil, "break") == 0)
                    {
                        break;
                    }
                }
                printf("==Files Listed==\n");
                // continue;
                /* code */
            }
            else if (strcmp(command, "download") == 0)
            {
                printf("[Server] download\n");
                printf("Input the problem name: ");

                char filename[30], status[30];
                filename[0] = '\0';
                scanf("%s", filename);
                write(sockfd, filename, sizeof(filename));
                // read the ststus
                read(sockfd, status, sizeof(status));
                printf("==%s==\n", status);
            }
            else if (strcmp(command, "submit") == 0)
            {
                printf("[Server] submit\n");
                char problemName[30], answerPath[30];
                char Response[30];
                printf("Input Problem name & answer path: ");

                scanf("%s %s", problemName, answerPath);
                write(sockfd, problemName, sizeof(problemName));
                write(sockfd, answerPath, sizeof(answerPath));
                read(sockfd, response, sizeof(response));
                printf("%s\n", response);
                /* code */
            }
        }

        // bzero(buffer, sizeof(buffer));
        // scanf("%s", buffer);
        // write(sockfd, buffer, sizeof(buffer));
        // bzero(buffer, sizeof(buffer));
        // read(sockfd, buffer, sizeof(buffer));
        // printf("From Server : %s", buffer);
    }
}
```

Dalam fungsi func, user akan diprompt untuk memasukan 3 opsi: Exit, Login, Register layaknya program server. Input pertama yang dibaca adalah opsi. Kemudian, client akan membaca input user dan password sesuai pilihan register atau login. Sesudah itu while loop yang berisi command. Layaknya server, user juga memiliki 4 command:

1. add

   Command ini berguna untuk melakukan submit answer baru ke server. Pada client terdapt 4 path yang diminta: judul, deskripsi, input, dan output. Keempat path ini akan diteruskan ke server untuk diproses.

2. see

   Command ini berguna untuk melihat list problem. Untuk melakukan hal tersebut dilakukan while loop yang akan membaca/read server sampai stop signal(pada program ini stop signal aldaah string "break").

3. download

   Command ini berugna untuk memberitahu server file problem mana yang ingin didowlaod oleh user, input yang diminta adalah judul problem yang akan diteruskan ke server dengan write.

4. submit

   Command ini berugna untuk melakukan submit problem, input yang diminta hanya judul problem dan path answer(output) dari user.

## 3. Nami

Soal ini memiliki 3 buah file: 1 file Untuk main code dan 2 lainnya dalam folder untuk Client atau Server.

### Soal3

Dalam file ini yang pertama dilakukan adalah pindah working directory ke /home/osboxes/shift3/hartakarun/, kemudian tiap file akan dibaca secara sekuensial dan dibuat thread untuk organize masing masing file.

#### Organizefile

Pada dasarnya, fungsi ini akan menerima nama dari tiap file dalam directory harta karun. Tiap file akan diambil file extensionnya dan dibuat folder yang sesuai dengan extension tersebut. Dalam kasus dimana tidak ada extension(destination hanya berisi string ./), akan dibuat folder bernama unknown.

#### get_filename_ext

```c
const char *get_filename_ext(const char *filename)
{
    const char *dot = strrchr(filename, '.');
    if (!dot || dot == filename)
        return "";
    return dot;
}
```

Fungsi ini menerima filename yang akan dipisah dengan fungsi strrchr yang akan mereturn pointer ke bagian string extension filenya.

#### safe_create_dir

```c
static void safe_create_dir(const char *dir)
{
    if (mkdir(dir, 0755) < 0)
    {
        if (errno != EEXIST)
        {
            perror(dir);
            exit(1);
        }
    }
}
```

Fungsi ini menerima nama directory dan akan membuat direcotry sesuai dengan direcory tersebut.

### Client

#### Main

```c
int main()
{

    char *ip = "127.0.0.1";
    int port = 8080;
    int e;
    CreateFile();
    int sockfd;
    struct sockaddr_in server_addr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("[-]Error in socket");
        exit(1);
    }
    printf("[Success]Server socket created successfully.\n");

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = port;
    server_addr.sin_addr.s_addr = inet_addr(ip);

    e = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (e == -1)
    {
        perror("[-]Error in socket");
        exit(1);
    }
    printf("[Success]Connected to Server.\n");
    char cmd[30];

    Send(sockfd);
    printf("[Success]File data sent successfully.\n");

    printf("[Client]Closing the connection.\n");
    close(sockfd);

    return 0;
}
```

Pada file client, yang ingin dilakukan adalah mengirim file hartakarun.zip ke client. Untuk mencapai hal tersebut dibuatlah socket. Lalu, server address akan dipass ke server address. Akhirnya socket yang dibuat dengan address akan di mencoba untuk disambungkan ke server dan menjalankan Fungsi send;

#### Send

```c
void Send(int sockfd)
{
    char buff[MAX];
    int n;
    for (;;)
    {
        bzero(buff, sizeof(buff));
        n = 0;
        printf("Enter command: \n");
        while ((buff[n++] = getchar()) != '\n')
            ;

        if ((strncmp(buff, "send", 4)) == 0)
        {
            char name[30] = "";
            scanf("%s", name);
            write(sockfd, buff, sizeof(buff));
            send_file(name, sockfd);
            // write(sockfd, buff, sizeof(buff));

            printf("Client Sent...\n");
            // break;
        }
        if ((strncmp(buff, "exit", 4)) == 0)
        {
            printf("Client Exit...\n");
            break;
        }
        else
        {
            printf("invalid command\n");
            break;
        }
        // write(sockfd, buff, sizeof(buff));
    }
    // send_file(name, sockfd);
}
```

Untuk melakukan komunikasi ke server, diperlukan fungsi write dan read, dimana kedua fungsi ini harus dilakukan bergantian pada server dan client. Fungsi send akan menerima id socket. Dalam fungsi ini terdapat for loop yang berjalan unlimited untuk menerima command dari user.
Command yang dapat dijalankan adalah:

1. send

   Jika user mengirim string yang sesuai dengan send maka user akan diprompt ekmbali untuk memasukan nama file ayng akan dikirim. Kemudian nama file ini akan diwrite ke socket. Hasil wirte akan kemudian
   ditangkap oleh server. Sementara itu, fungsi send_file akan dijalankan.

2. exit

   Jika user mengirim string yang sesuai dengan exit maka client akan break dari for loop dan exit.

#### Send_file

```c
void send_file(const char *filename, int sockfd)
{
    int n;
    char data[1024] = {0};

    FILE *fp;
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        perror("[-]Error in reading file.");
        exit(1);
    }

    while (fgets(data, SIZE, fp) != NULL)
    {
        if (send(sockfd, data, sizeof(data), 0) == -1)
        {
            perror("[-]Error in sending file.");
            exit(1);
        }
        bzero(data, SIZE);
    }
    close(fp);
}
```

Fungsi ini berguna untuk mengirimkan file ke destination socket. Pertama, file akan diread dengan fgets dan tiap data dikirim samapi file meneumui EOF.

#### CreateFile

```c
void CreateFile()
{

    int createstate;
    pid_t pid = fork();
    if (pid == 0)
    {
        execl("/bin/zip", "/bin/zip", "./hartakarun.zip", "/home/osboxes/shift3/hartakarun", NULL);

        /* code */
    }
    else
    {
        while (wait(&createstate) > 0)
        {
            /* code */
        }
    }
}
```

Fungsi ini berguna untuk menzip dan membuat file hartakarun.zip

### Server

#### Main

```c
int main()
{
    char *ip = "127.0.0.1";
    int port = 8080;
    int e;

    int sockfd, new_sock;
    struct sockaddr_in server_addr, new_addr;
    socklen_t addr_size;
    char buffer[SIZE];

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("[-]Error in socket");
        exit(1);
    }
    printf("[+]Server socket created successfully.\n");

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = port;
    server_addr.sin_addr.s_addr = inet_addr(ip);

    e = bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (e < 0)
    {
        perror("[-]Error in bind");
        exit(1);
    }
    printf("[+]Binding successfull.\n");

    if (listen(sockfd, 10) == 0)
    {
        printf("[+]Listening....\n");
    }
    else
    {
        perror("[-]Error in listening");
        exit(1);
    }

    addr_size = sizeof(new_addr);
    new_sock = accept(sockfd, (struct sockaddr *)&new_addr, &addr_size);
    Accept(new_sock);

    return 0;
}
```

Yang ingin dilakukan oleh file server adalah menerima koneksi sehingga yang pertama dilakukan dalah emndeklarasi port, ip, dan socket yang berkaitan. Selanjutnya, socket akan dibuat dan server address akan diassign. Berkebalikan dengan client yang menggunakan fungsi connect, server menggunakan fungsi bind untuk listen terhadap socket tersebut. Jika sukses, server akan melakukan accept dan fungsi accept akan dijalankan.

#### Accept

```c
void Accept(int connfd)
{
    char buff[MAX];
    int n;
    // infinite loop for chat
    for (;;)
    {
        bzero(buff, MAX);

        read(connfd, buff, sizeof(buff));

        if (strncmp("send", buff, 4) == 0)
        {
            write_file(connfd);

            printf("Server Accepting file...\n");
            break;
        }
        // if msg contains "Exit" then server exit and chat ended.
        if (strncmp("exit", buff, 4) == 0)
        {
            printf("Server Exit...\n");
            break;
        }
        // break;
    }
}
```

Fungsi ini berguna untuk menghandle client yang telah terkoneksi. Didalamnya terdapat for loop yang berjalan secarat terus-menerus. Dalam for loop iini akan menjalankan fungsi read untuk membaca command dari client.
Jika command berupa send, maka server akan menerima file dengan fungsi write_file. Jika command berupa exit, server akan ikut exit.

#### Send_file

```c
void write_file(int sockfd)
{
    int n;
    FILE *fp;
    char buffer[SIZE];
    printf("writing to file\n");
    fp = fopen("hartakarun.zip", "w");
    while (1)
    {
        n = recv(sockfd, buffer, SIZE, 0);
        if (n <= 0)
        {
            break;
            return;
        }
        fprintf(fp, "%s", buffer);
        bzero(buffer, SIZE);
    }
    printf("[+]Data written in the file successfully.\n");

    return;
}
```

Fungsi ini berguna untuk menerima file dan membaca data kiriman ke file tersebut. Dalam while loop, terdpat fungsi recv() yang akan menerima byte kiriman dari client dan menempatkannya di file server.

# Masalah dan Dokumentasi

## 1. Novak

![nomer1](https://gitlab.com/uploads/-/system/personal_snippet/2295877/cdce96a8780a230a05643464e0f8df45/1.png)

- Thread yang menunggu satu sama lain sedikit membingungkan
- Permintaan soal untuk unzip, add dan zip

## 2. Bluemary

![nomer2](https://gitlab.com/uploads/-/system/personal_snippet/2295877/0d7dc4513db9a22959d34cac51566687/2.png)

- Soal sangat kompleks dan memerlukan threading+client server communication
- Komunikasi sangat sulit untuk dilakukan, perlu read dan write sekuensial client server

## 3. Nami

![nomer3](https://gitlab.com/uploads/-/system/personal_snippet/2295877/49472e6bcc5d713b019d719b827d98e2/3.png)

- Kesalahan memanggil fungsi 2 kali, sehingga folder harta karun langsung terkirim saat pertama dipanggil
