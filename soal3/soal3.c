#include <sys/resource.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdint.h>
#include <dirent.h>
#include <ctype.h>
#include <zip.h>
#include <errno.h>
#include <fcntl.h>
#include <zlib.h>

#define ERR_SIZE 256
#define COPY_BUF_SIZE 2048
const char *prg;
int Unzipstatus;

const char *get_filename_ext(const char *filename)
{
    const char *dot = strrchr(filename, '.');
    if (!dot || dot == filename)
        return "";
    return dot;
}

static void safe_create_dir(const char *dir)
{
    if (mkdir(dir, 0755) < 0)
    {
        if (errno != EEXIST)
        {
            perror(dir);
            exit(1);
        }
    }
}

#define UNZIP_OK 0
#define UNZIP_ERROPEN 1
#define UNZIP_ERRNOTFOUND 2
#define UNZIP_ERRCREATE 3
#define UNZIP_ERRREAD 4
#define UNZIP_ERRWRITE 5

void Extract_Zip(char *zipfile, char *filename, char *outfile)
{

    // unzFile uf = 0;
    // FILE *fout = 0;
    // int result, bytes;
    // char buffer[1024];

    // uf = unzOpen(zipfile);
    // if (!uf)
    // {
    //     printf("Can not open file '%s' !\n", zipfile);
    //     return UNZIP_ERROPEN;
    // }

    // if (unzLocateFile(uf, filename, 0) != UNZ_OK)
    // {
    //     printf("Can not find '%s' inside the zipfile !\n", filename);
    //     unzCloseCurrentFile(uf);
    //     return UNZIP_ERRNOTFOUND;
    // }

    // if (unzOpenCurrentFile(uf) != UNZ_OK)
    // {
    //     printf("Can not open '%s' inside the zipfile !\n", filename);
    //     unzCloseCurrentFile(uf);
    //     return UNZIP_ERRNOTFOUND;
    // }

    // fout = fopen(outfile, "wb");
    // if (!fout)
    // {
    //     printf("Can not create file '%s' !\n", outfile);
    //     unzCloseCurrentFile(uf);
    //     return UNZIP_ERRCREATE;
    // }

    // result = UNZIP_OK;
    // for (;;)
    // {
    //     bytes = unzReadCurrentFile(uf, buffer, sizeof(buffer));
    //     if (bytes == 0)
    //         break; /* finish */
    //     if (bytes < 0)
    //     {
    //         printf("Error reading %s %d!\n", filename, bytes);
    //         result = UNZIP_ERRREAD;
    //         break;
    //     }
    //     else if (fwrite(buffer, 1, bytes, fout) != bytes)
    //     {
    //         printf("error in writing extracted file\n");
    //         result = UNZIP_ERRWRITE;
    //         break;
    //     }
    // }

    // fclose(fout);
    // unzCloseCurrentFile(uf);

    // return result;
}

pthread_t tid[100];

char *toLower(char *s)
{
    for (char *p = s; *p; p++)
        *p = tolower(*p);
    return s;
}

void *OrganizeFile(void *arg)
{
    char *originalfile = arg;
    char filename[50] = "";
    strcpy(filename, originalfile);
    char *ext = get_filename_ext(filename);
    ext = toLower(ext);
    if (ext != NULL)
    {
        // printf("\n%s", ep->d_name);
        /* code */
        char dest_name[50] = "./";
        strcat(dest_name, ext + 1);

        char source_name[50] = "./";
        // printf(" %s ", ep->d_name);

        strcat(source_name, originalfile);
        // printf("%s\n", dest_name);
        // mk dir here

        // if here
        if (strlen(dest_name) == 2)
        {
            // printf("%s sxxs %s\n", filename, ext + 1);
            // udah ada satu /
            strcat(dest_name, "Unknown");
            mkdir(dest_name, 0777);
            // printf("%s\n", dest_name);
            strcat(dest_name, "/");
            strcat(dest_name, originalfile);

            // continue;
        }
        else
        {
            /* code */
            strcat(dest_name, "/");
            mkdir(dest_name, 0777);
            strcat(dest_name, originalfile);
        }

        // rename here
        rename(source_name, dest_name);
        // printf("\n%s %s\n", source_name, dest_name);
        // printf("\n%s\n", originalfile);
    }
}

void main()
{
    // mkdir("/home/osboxes/shift3", 0777);
    //  execl("/bin/unzip", "/bin/unzip", "/home/osboxes/Downloads/hartakarun.zip", "-d", "/home/osboxes/shift3", NULL);
    printf("Initializing\n");
    // Extract_Zip("/home/osboxes/Downloads/hartakarun.zip", 0);

    chdir("/home/osboxes/shift3/hartakarun/");
    DIR *dp;
    struct dirent *ep;

    char path[100] = "/home/osboxes/shift3/hartakarun";

    dp = opendir(path);

    if (dp != NULL)
    {
        int i = 0;
        while ((ep = readdir(dp)))
        {
            pthread_create(&(tid[i]), NULL, OrganizeFile, ep->d_name);
            pthread_join(tid[i], NULL);
            i++;
        }

        (void)closedir(dp);
    }
    else
        perror("Couldn't open the directory");

    return 0;
}