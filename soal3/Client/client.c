#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#define MAX 80
#define PORT 8080
#define SA struct sockaddr
#define SIZE 1024

void Send(int sockfd)
{
    char buff[MAX];
    int n;
    for (;;)
    {
        bzero(buff, sizeof(buff));
        n = 0;
        printf("Enter command: \n");
        while ((buff[n++] = getchar()) != '\n')
            ;

        if ((strncmp(buff, "send", 4)) == 0)
        {
            char name[30] = "";
            scanf("%s", name);
            write(sockfd, buff, sizeof(buff));
            send_file(name, sockfd);
            // write(sockfd, buff, sizeof(buff));

            printf("Client Sent...\n");
            // break;
        }
        if ((strncmp(buff, "exit", 4)) == 0)
        {
            printf("Client Exit...\n");
            break;
        }
        else
        {
            printf("invalid command\n");
            break;
        }
        // write(sockfd, buff, sizeof(buff));
    }
    // send_file(name, sockfd);
}

void send_file(const char *filename, int sockfd)
{
    int n;
    char data[1024] = {0};

    FILE *fp;
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        perror("[-]Error in reading file.");
        exit(1);
    }

    while (fgets(data, SIZE, fp) != NULL)
    {
        if (send(sockfd, data, sizeof(data), 0) == -1)
        {
            perror("[-]Error in sending file.");
            exit(1);
        }
        bzero(data, SIZE);
    }
    close(fp);
}

void CreateFile()
{

    int createstate;
    pid_t pid = fork();
    if (pid == 0)
    {
        execl("/bin/zip", "/bin/zip", "./hartakarun.zip", "/home/osboxes/shift3/hartakarun", NULL);

        /* code */
    }
    else
    {
        while (wait(&createstate) > 0)
        {
            /* code */
        }
    }
}

int main()
{

    char *ip = "127.0.0.1";
    int port = 8080;
    int e;
    CreateFile();
    int sockfd;
    struct sockaddr_in server_addr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("[-]Error in socket");
        exit(1);
    }
    printf("[Success]Server socket created successfully.\n");

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = port;
    server_addr.sin_addr.s_addr = inet_addr(ip);

    e = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (e == -1)
    {
        perror("[-]Error in socket");
        exit(1);
    }
    printf("[Success]Connected to Server.\n");
    char cmd[30];

    Send(sockfd);
    printf("[Success]File data sent successfully.\n");

    printf("[Client]Closing the connection.\n");
    close(sockfd);

    return 0;
}