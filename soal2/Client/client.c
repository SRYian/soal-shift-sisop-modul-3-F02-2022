#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <stdbool.h>
#define MAX 80
#define PORT 8888
#define SA struct sockaddr

void func(int sockfd)
{
    char buffer[MAX];
    char username[MAX];
    char password[MAX];
    char response[MAX];
    char command[MAX];
    char n[MAX];
    for (;;)
    {
        username[0] = '\0';
        password[0] = '\0';
        n[0] = '\0';
        response[0] = '\0';
        command[0] = '\0';
        printf("\tOptions\n0. Exit\n1. Login\n2. Register\n");

        // send(sockfd, n, sizeof(n), 0);
        scanf("%s", n);
        write(sockfd, n, sizeof(n));
        if (strcmp(n, "0") == 0)
        {
            printf("Client Exit...\n");
            break;
        }
        scanf("%s", username);
        scanf("%s", password);
        write(sockfd, username, sizeof(username));
        write(sockfd, password, sizeof(password));
        read(sockfd, response, sizeof(response)); // read server status
        if (strcmp(response, "0") == 0)
        {
            printf("%s", response);
            continue;
        }
        while (strcmp(command, "quit") != 0)
        {
            printf("Input command: \n");

            scanf("%s", command);
            write(sockfd, command, sizeof(command));
            if (strcmp(command, "add") == 0)
            {
                printf("[Server] add\n");

                char Judul[30] = "";
                char Desc[30] = "";
                char Input[30] = "";
                char Output[30] = "";

                printf("Judul problem: ");
                scanf("%s", Judul);
                printf("Filepath description.txt: ");
                scanf("%s", Desc);

                printf("Filepath input.txt: ");
                scanf("%s", Input);

                printf("Filepath output.txt: ");
                scanf("%s", Output);

                write(sockfd, Judul, sizeof(command));
                write(sockfd, Desc, sizeof(command));
                write(sockfd, Input, sizeof(command));
                write(sockfd, Output, sizeof(command));
            }
            else if (strcmp(command, "see") == 0)
            {
                printf("[Server] see\n");
                char hasil[80];
                while (1)
                {
                    hasil[0] = '\0';
                    read(sockfd, hasil, sizeof(hasil));
                    printf("%s\n", hasil);
                    if (strcmp(hasil, "break") == 0)
                    {
                        break;
                    }
                }
                printf("==Files Listed==\n");
                // continue;
                /* code */
            }
            else if (strcmp(command, "download") == 0)
            {
                printf("[Server] download\n");
                printf("Input the problem name: ");

                char filename[30], status[30];
                filename[0] = '\0';
                scanf("%s", filename);
                write(sockfd, filename, sizeof(filename));
                // read the ststus
                read(sockfd, status, sizeof(status));
                printf("==%s==\n", status);
            }
            else if (strcmp(command, "submit") == 0)
            {
                printf("[Server] submit\n");
                char problemName[30], answerPath[30];
                char Response[30];
                printf("Input Problem name & answer path: ");

                scanf("%s %s", problemName, answerPath);
                write(sockfd, problemName, sizeof(problemName));
                write(sockfd, answerPath, sizeof(answerPath));
                read(sockfd, response, sizeof(response));
                printf("%s\n", response);
                /* code */
            }
        }

        // bzero(buffer, sizeof(buffer));
        // scanf("%s", buffer);
        // write(sockfd, buffer, sizeof(buffer));
        // bzero(buffer, sizeof(buffer));
        // read(sockfd, buffer, sizeof(buffer));
        // printf("From Server : %s", buffer);
    }
}

int main()
{
    int sockfd, connfd;
    struct sockaddr_in srvaddr, cli;
    char st[30], us[30];
    // scanf("%s %s", us, st);
    // Register(us, st);
    // Login(us, st);
    // execl("/bin/pwd", "/bin/pwd", NULL);

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&srvaddr, sizeof(srvaddr));

    // assign IP, PORT
    srvaddr.sin_family = AF_INET;
    srvaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    srvaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (SA *)&srvaddr, sizeof(srvaddr)) != 0)
    {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");

    // function for chat
    func(sockfd);

    // close the socket
    close(sockfd);
}