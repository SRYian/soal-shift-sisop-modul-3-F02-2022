#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <unistd.h>
#include <dirent.h>
#include <pthread.h>

#define MAX 80
#define PORT 8888
#define SA struct sockaddr

bool PassValidator(char *pass)
{
    char ch;
    int i, number, special, capital, small;
    number = special = capital = small = 0;
    for (i = 0; pass[i] != '\0'; i++)
    {
        if (pass[i] >= '0' && pass[i] <= '9')
            number = 1;
        if (pass[i] >= 'a' && pass[i] <= 'z')
            small = 1;
        if (pass[i] >= 'A' && pass[i] <= 'Z')
            capital = 1;
        if (pass[i] == '!' || pass[i] == '@' || pass[i] == '#' || pass[i] == '$' || pass[i] == '%' || pass[i] == '*')
            special = 1;
    }
    if (number == 0 || special == 0 || capital == 0 || small == 0 || strlen(pass) < 6)
    {
        printf("\nInvalid Password\n");
        return false;
    }
    else
    {
        printf("\nValid Password\n");
        return true;
    }
}
bool UserValidator(char *username)
{
    char *string[50];
    FILE *in_file;
    in_file = fopen("users.txt", "a+");
    if (in_file == NULL)
    {
        printf("Error file missing\n");
        exit(-1);
    }
    while (!feof(in_file))
    {
        fscanf(in_file, "%s", string);
        if (strstr(string, username))
        {
            printf("Username already exist\n");
            fclose(in_file);
            return false;
        }
    }
    fclose(in_file);
    return true;
}

void Register(char *username, char *password, int connfd)
{
    char compound[160] = "";
    FILE *in_file;
    in_file = fopen("users.txt", "a+");
    if (in_file == NULL)
    {
        printf("Error file missing\n");
        exit(-1);
    }
    if (UserValidator(username) && PassValidator(password))
    {

        strcat(compound, username);
        strcat(compound, "-");
        strcat(compound, password);
        fprintf(in_file, "%s\n", compound);
        printf("Register Success!");
        /* code */
    }
    else
    {
        printf("Failed to verify, try again!");
    }
    fclose(in_file);
}

bool Login(char *username, char *password, int connfd)
{
    char *string[50];
    char compound[160] = "";
    FILE *in_file;
    in_file = fopen("users.txt", "a+");
    if (in_file == NULL)
    {
        printf("Error file missing\n");
        exit(-1);
    }

    strcat(compound, username);
    strcat(compound, "-");
    strcat(compound, password);
    // fprintf(in_file, "%s\n", compound);
    while (!feof(in_file))
    {
        fscanf(in_file, "%s", string);

        if (strstr(string, compound))
        {

            printf("Login Success\n");
            fclose(in_file);
            return true;
        }
    }
    printf("Login Failed!");
    /* code */

    printf("Failed to verify Login try again!");

    fclose(in_file);
    return false;
}

void CreateFile()
{
    FILE *in_file;
    in_file = fopen("problems.tsv", "a+");
    if (in_file == NULL)
    {
        printf("Error file missing\n");
        exit(-1);
    }
    fclose(in_file);
}

void AddFile(char *author, char *judul, char *desc, char *input, char *output)
{

    struct stat st = {0};

    if (stat(judul, &st) == -1)
    {
        mkdir(judul, 0700);
    }
    char path[100] = "Server/";
    char path_cpy[100] = "";
    chdir("..");

    strcat(path, judul);
    strcat(path, "/");
    strcpy(path_cpy, path);

    // execl("/bin/pwd", "/bin/pwd", NULL);

    FILE *DescFile;
    DescFile = fopen(desc, "a+");

    FILE *InputFile;
    InputFile = fopen(input, "r+");

    FILE *OutputFile;
    OutputFile = fopen(output, "r+");

    FILE *SrvDesc;
    FILE *SrvIn;
    FILE *SrvOut;
    FILE *problemsTSV;

    strcat(path, "description.txt");
    SrvDesc = fopen(path, "w");
    strcpy(path, path_cpy);
    strcat(path, "input.txt");
    SrvIn = fopen(path, "w");
    strcpy(path, path_cpy);
    strcat(path, "output.txt");
    SrvOut = fopen(path, "w");

    // printf("%s", DescFile);
    problemsTSV = fopen("Server/problems.tsv", "a+");
    char Contents[300] = "";

    while (fscanf(DescFile, "%s", Contents) == 1)
    {
        fprintf(SrvDesc, "%s\n", Contents);
    }
    Contents[300] = '\0';
    while (fscanf(InputFile, "%s", Contents) != EOF)
    {
        fprintf(SrvIn, "%s\n", Contents);
    }
    Contents[0] = '\0';
    while (fscanf(OutputFile, "%s", Contents) != EOF)
    {
        fprintf(SrvOut, "%s\n", Contents);
    }
    Contents[0] = '\0';
    strcat(Contents, judul);

    strcat(Contents, "\t");
    strcat(Contents, author);
    printf("jdudl: %s\n", judul);

    printf("%s\n", Contents);
    fprintf(problemsTSV, "%s\n", Contents);

    fclose(DescFile);
    fclose(InputFile);
    fclose(OutputFile);
    fclose(SrvDesc);
    fclose(SrvIn);
    fclose(SrvOut);
    fclose(problemsTSV);

    chdir("Server");
    // execl("/bin/pwd", "/bin/pwd", NULL);
}

void SeeFile(int conn)
{
    FILE *problemsTSV;
    problemsTSV = fopen("problems.tsv", "a+");
    char problem[100], auth[100];
    while (fscanf(problemsTSV, "%s\t%s\n", problem, auth) != EOF)
    {
        strcat(problem, " by ");
        strcat(problem, auth);
        char fullname[80] = "";
        printf("%s\n", problem);
        strcpy(fullname, problem);
        write(conn, fullname, sizeof(fullname));
    }
    write(conn, "break", sizeof("break"));
    printf("break otw\n");

    fclose(problemsTSV);
}

void DownloadFile(int connfd, char *foldername)
{
    DIR *dp;
    struct dirent *ep;
    chdir("..");
    char path[100] = "Server/";
    strcat(path, foldername);
    dp = opendir(path);
    strcat(path, "/");

    // create folder in client
    char clientFolder[100] = "Client/";
    strcat(clientFolder, foldername);
    mkdir(clientFolder, 0700);
    char freshpath[100] = "";
    char filePath[100] = "Client/";
    strcat(filePath, foldername);
    strcat(filePath, "/");
    strcpy(freshpath, filePath);
    printf("%s", filePath);
    char freshpathOR[100] = "";
    char filePathOR[100] = "Server/";
    strcat(filePathOR, foldername);
    strcat(filePathOR, "/");
    strcpy(freshpathOR, filePathOR);
    printf("%s", filePathOR);

    if (dp != NULL)
    {
        while ((ep = readdir(dp)))
        {
            if (strcmp(ep->d_name, "description.txt") == 0 || strcmp(ep->d_name, "input.txt") == 0)
            {
                // pathfile[0] = '\0';
                strcat(filePathOR, ep->d_name);

                strcat(filePath, ep->d_name);
                FILE *fromfile,
                    *tofile;
                fromfile = fopen(filePathOR, "a+");
                tofile = fopen(filePath, "a+");
                char str[100];

                while (fscanf(fromfile, "%s\n", str) != EOF)
                {
                    fprintf(tofile, "%s\n", str);
                    str[0] = '\0';
                }

                strcpy(filePath, freshpath);
                strcpy(filePathOR, freshpathOR);
                puts(ep->d_name);
                fclose(fromfile);
                fclose(tofile);
                /* code */
            }
        }
        write(connfd, "Download Success!", sizeof("Download Success!"));
        (void)closedir(dp);
    }
    else
    {
        write(connfd, "Directory error!", sizeof("Directory error!"));

        perror("Couldn't open the directory");
    }

    chdir("Server");
}

bool compareFiles(FILE *file1, FILE *file2)
{
    char str1[30] = "", str2[30] = "";
    while ((fscanf(file2, "%s\n", str2) != EOF) && (fscanf(file1, "%s\n", str1) != EOF))
    {

        // printf("str1: %s\n", str1);
        // printf("str2: %s\n", str2);

        if (strcmp(str1, str2) != 0)
        {
            return false;
        }
        str1[0] = '\0';
        str2[0] = '\0';
    }

    return true;
}

bool compareFileLenght(FILE *file1, FILE *file2)
{
    char str1[30] = "", str2[30] = "";
    int ln1 = 0, ln2 = 0;
    while (fscanf(file2, "%s\n", str2) != EOF)
    {
        ln1++;

        printf("str2: %s\n", str2);
    }
    while (fscanf(file1, "%s\n", str1) != EOF)
    {
        ln2++;
        printf("str1: %s\n", str1);
        /* code */
    }
    if (ln1 == ln2)
    {
        return true;
    }
    else
        return false;
}

void SubmitFile(int connfd, char *foldername, char *cloutputfile)
{
    chdir("..");
    FILE *clout, *srvans;
    clout = fopen(cloutputfile, "a+");

    char path[100] = "Server/";
    strcat(path, foldername);
    // assume every problems has the same out name
    strcat(path, "/output.txt");
    printf("%s %s\n", path, cloutputfile);
    srvans = fopen(path, "a+");

    if (compareFileLenght(clout, srvans) && compareFiles(clout, srvans))
    {
        printf("Problem %s AC\n", foldername);
        write(connfd, "AC", sizeof("AC"));
    }
    else
    {
        printf("Problem %s WA\n", foldername);
        write(connfd, "WA", sizeof("WA"));

        /* code */
    }

    chdir("Server");
}

// Function designed for chat between client and server.
void *func(void *tmp)
{
    int connfd = *(int *)tmp;
    char buffer[MAX];
    char username[MAX];
    char password[MAX];
    char n[MAX];
    char command[MAX];

    for (;;)
    {
        username[0] = '\0';
        password[0] = '\0';
        n[0] = '\0';
        command[0] = '\0';
        read(connfd, n, sizeof(n));
        if (strcmp(n, "0") == 0)
        {
            printf("Server Exit...\n");
            pthread_exit(NULL);
            break;
        }
        read(connfd, username, sizeof(username));
        read(connfd, password, sizeof(password));
        printf("%s %s %s\n", n, username, password);

        if (strcmp(n, "1") == 0)
        {
            if (Login(username, password, connfd))
            {
                /* code */
                write(connfd, "1", sizeof("1"));
            }
        }
        else if (strcmp(n, "2") == 0)
        {
            Register(username, password, connfd);
            write(connfd, "1", sizeof("1"));

            /* code */
        }
        else
        {
            write(connfd, "0", sizeof("0"));
            continue;
        }
        // read(connfd, command, sizeof(command));
        while (strcmp(command, "quit") != 0)
        {
            read(connfd, command, sizeof(command));
            if (strcmp(command, "add") == 0)
            {
                printf("[Server] add\n");

                char Judul[30] = "";
                char Desc[30] = "";
                char Input[30] = "";
                char Output[30] = "";

                read(connfd, Judul, sizeof(command));
                read(connfd, Desc, sizeof(command));
                read(connfd, Input, sizeof(command));
                read(connfd, Output, sizeof(command));
                printf("Judul problem: %s\n", Judul);
                printf("Filepath description.txt: %s\n", Desc);
                printf("Filepath input.txt: %s\n", Input);
                printf("Filepath output.txt: %s\n", Output);
                AddFile(username, Judul, Desc, Input, Output);
            }
            else if (strcmp(command, "see") == 0)
            {
                printf("[Server] see\n");
                SeeFile(connfd);
                /* code */
            }
            else if (strcmp(command, "download") == 0)
            {
                printf("[Server] download\n");
                char filename[30];
                filename[0] = '\0';
                read(connfd, filename, sizeof(filename));
                DownloadFile(connfd, filename);
                /* code */
            }
            else if (strcmp(command, "submit") == 0)
            {
                printf("[Server] submit\n");
                char problemName[30], answerPath[30];
                read(connfd, problemName, sizeof(problemName));
                read(connfd, answerPath, sizeof(answerPath));
                SubmitFile(connfd, problemName, answerPath);
                /* code */
            }
        }

        // write(connfd, buffer, sizeof(buffer));
    }
}

pthread_t tid[10];
int ctr = 0;
// Driver function
int main()
{
    int sockfd, connfd, len;
    struct sockaddr_in srvaddr, cli;
    // CreateFile();
    // AddFile("YIAN", "pavo", "Client/desc.txt", "Client/input.txt", "Client/output.txt");
    // SeeFile();
    printf("asdadsda\n");
    // DownloadFile("Shirobako");
    // SubmitFile("Shirobako", "Client/output.txt");
    // execl("/bin/pwd", "/bin/pwd", NULL);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&srvaddr, sizeof(srvaddr));

    srvaddr.sin_family = AF_INET;
    srvaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    srvaddr.sin_port = htons(PORT);

    if ((bind(sockfd, (SA *)&srvaddr, sizeof(srvaddr))) != 0)
    {
        printf("socket bind failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully binded..\n");

    if ((listen(sockfd, 5)) != 0)
    {
        printf("Listen failed...\n");
        exit(0);
    }
    else
        printf("Server listening..\n");
    len = sizeof(cli);

    // func(connfd);
    while (1)
    {
        connfd = accept(sockfd, (SA *)&cli, &len);
        if (connfd < 0)
        {
            printf("server accept failed...\n");
            exit(0);
        }
        else
            printf("server accept the client...\n");

        pthread_create(&(tid[ctr]), NULL, &func, &connfd);
        printf("dsadadsadsa");
        pthread_join(tid[ctr], NULL);
        ctr++;
        /* code */
    }

    close(sockfd);
}